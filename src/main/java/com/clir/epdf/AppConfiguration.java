package com.clir.epdf;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class AppConfiguration {
    @Value("${xmlConfigurationFile}") private String xmlConfigurationFile;
    @Value("${templateFolder}") private String templateFolder;
}
