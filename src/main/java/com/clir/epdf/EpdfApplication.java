package com.clir.epdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpdfApplication {
	public static void main(String[] args) {
		SpringApplication.run(EpdfApplication.class, args);
	}
}
