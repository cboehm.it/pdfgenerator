package com.clir.epdf;

import com.clir.epdf.dto.PdfGenerationResult;
import com.clir.epdf.exception.ConfigurationException;
import com.clir.epdf.exception.PdfGenerationException;
import com.clir.epdf.service.PdfService;
import com.clir.epdf.service.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class PdfController {
    private Logger log = LoggerFactory.getLogger(PdfController.class);

    private PdfService pdfService;
    private ConfigurationService configurationService;

    public PdfController(PdfService pdfService, ConfigurationService configurationService) {
        this.pdfService = pdfService;
        this.configurationService = configurationService;
    }

    @GetMapping("/refresh")
    public HttpEntity refresh(){
        log.debug("Refreshing Configuration");

        try {
            configurationService.refreshConfiguration();
            return ResponseEntity.ok().build();
        } catch (ConfigurationException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/pdf/{config}/{communicationCode}")
    public HttpEntity generate(@PathVariable String config, @PathVariable String communicationCode){
        log.debug(String.format("Pdf Generation called for Configuration %s and Communication-Code %s", config, communicationCode));

        try {
            PdfGenerationResult result = pdfService.generatePdf(config, communicationCode);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            headers.setContentDispositionFormData(result.getDownloadName(), result.getDownloadName());
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

            log.debug("Pdf Generation was successful (OK)");
            return ResponseEntity.ok().headers(headers).body(result.getPdf());
        } catch (PdfGenerationException ex){
            log.debug("Pdf Generation was not successful (NOT OK): " + ex.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (Exception ex){
            ex.printStackTrace();
            log.debug("Pdf Generation ended with an unexpected error (NOT OK): " + ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}