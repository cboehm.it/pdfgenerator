package com.clir.epdf;

import java.util.Map;
import java.util.Properties;

public class Utilities {
    public static Properties convertMapToProperties(Map<String, Object> data){
        Properties p = new Properties();
        p.putAll(data);
        return p;
    }
}
