package com.clir.epdf.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Configuration {
    private String name;
    private String downloadName;
    private String template;
    private PersonalizationType personalizationType;
    private DataProvider dataProvider;
}
