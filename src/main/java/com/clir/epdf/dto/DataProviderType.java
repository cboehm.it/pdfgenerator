package com.clir.epdf.dto;

public enum DataProviderType {
    SQL,
    SQL_TEMPLATE
}
