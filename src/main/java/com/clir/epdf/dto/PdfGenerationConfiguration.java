package com.clir.epdf.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.util.List;

@NoArgsConstructor
@Data
@XmlRootElement(name="pdf")
@XmlAccessorType(XmlAccessType.FIELD)
public class PdfGenerationConfiguration {
    @XmlElementWrapper(name="configurations")
    @XmlElement(name="configuration")
    private List<Configuration> configurations;

    @XmlElementWrapper(name="sqls")
    @XmlElement(name="sql")
    private List<Sql> sqls;
}
