package com.clir.epdf.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PdfGenerationResult {
    private String downloadName;
    private byte[] pdf;
}
