package com.clir.epdf.exception;

public class ConfigurationNotFoundException extends PdfGenerationException {
    public ConfigurationNotFoundException(String configuration) {
        super("Configuration " + configuration + "could not be found");
    }
}
