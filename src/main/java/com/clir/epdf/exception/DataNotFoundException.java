package com.clir.epdf.exception;

public class DataNotFoundException extends PdfGenerationException {
    public DataNotFoundException(String communicationCode) {
        super("Communication-Data could " + communicationCode + " could not be found");
    }
}
