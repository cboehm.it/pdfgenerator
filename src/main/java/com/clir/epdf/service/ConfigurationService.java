package com.clir.epdf.service;

import com.clir.epdf.dto.Configuration;
import com.clir.epdf.dto.DataProviderType;
import com.clir.epdf.dto.PdfGenerationConfiguration;
import com.clir.epdf.exception.ConfigurationException;
import com.clir.epdf.AppConfiguration;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Map;
import java.util.Optional;

@Service
public class ConfigurationService {
    private Unmarshaller configurationUnmarshaller;
    private AppConfiguration appConfiguration;

    private PdfGenerationConfiguration pdfGenerationConfiguration;
    private Map<String, Configuration> configurationByName;

    public ConfigurationService(AppConfiguration appConfiguration) throws Exception {
        this.appConfiguration = appConfiguration;

        configurationUnmarshaller = JAXBContext.newInstance(PdfGenerationConfiguration.class).createUnmarshaller();
        loadConfiguration();
    }

    public void refreshConfiguration() throws ConfigurationException {
        this.loadConfiguration();
    }

    private void loadConfiguration() throws ConfigurationException {
        PdfGenerationConfiguration tempPdfGenerationConfiguration  = null;
        Map<String, Configuration> tempConfigurationByName = null;

        try {
            tempPdfGenerationConfiguration = (PdfGenerationConfiguration) configurationUnmarshaller.unmarshal(new File(appConfiguration.getXmlConfigurationFile()));

            for(Configuration conf : tempPdfGenerationConfiguration.getConfigurations()){
                tempConfigurationByName.put(conf.getName(), conf);
                conf.setTemplate(appConfiguration.getTemplateFolder() + conf.getTemplate());

                // Special logic for loading of SQLs as the structure allows linking
                if(conf.getDataProvider() != null && conf.getDataProvider().getType() != null && conf.getDataProvider().getType().equals(DataProviderType.SQL_TEMPLATE)){
                    conf.getDataProvider().setType(DataProviderType.SQL);
                    conf.getDataProvider().setVal(
                            tempPdfGenerationConfiguration.getSqls().stream()
                                    .filter(s -> s.getName().equals(conf.getDataProvider().getVal()))
                                    .findFirst()
                                    .orElseThrow(() -> new ConfigurationException("Sql with the defined name could not be found"))
                                    .getVal());
                }
            }
        } catch (JAXBException e) {
            throw new ConfigurationException("Marshalling of configuration was not possible", e);
        }

        this.pdfGenerationConfiguration = tempPdfGenerationConfiguration;
        this.configurationByName = tempConfigurationByName;
    }

    public Optional<Configuration> getConfiguration(String name){
        return Optional.of(configurationByName.get(name));
    }
}
