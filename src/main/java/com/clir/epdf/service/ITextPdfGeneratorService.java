package com.clir.epdf.service;


import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.clir.epdf.exception.PdfGenerationException;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Properties;

@Service
public class ITextPdfGeneratorService {
    public byte[] generatePdfFromAcroForm(File template, Properties data) throws PdfGenerationException {
        byte[] byteDocs;

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            PdfReader pdfReader = new PdfReader(template.getAbsolutePath());
            PdfWriter pdfWriter = new PdfWriter(outputStream);
            PdfDocument doc = new PdfDocument(pdfReader, pdfWriter);

            PdfAcroForm form = PdfAcroForm.getAcroForm(doc, true);

            for(Object key : data.keySet()){
                Object val = data.get(key);
                form.getField(key.toString()).setValue(val.toString());
            }

            form.flattenFields();
            doc.close();

            byteDocs = outputStream.toByteArray();
        } catch (Exception ex){
            throw new PdfGenerationException("Error while generating PDF", ex);
        }

        return byteDocs;
    }
}
