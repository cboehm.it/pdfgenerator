package com.clir.epdf.service;

import com.clir.epdf.dto.Configuration;
import com.clir.epdf.dto.PdfGenerationResult;
import com.clir.epdf.dto.PersonalizationType;
import com.clir.epdf.exception.ConfigurationNotFoundException;
import com.clir.epdf.exception.DataNotFoundException;
import com.clir.epdf.exception.PdfGenerationException;
import com.clir.epdf.dto.DataProviderType;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Properties;

@Service
public class PdfService {
    private ITextPdfGeneratorService iTextPdfGeneratorService;
    private SqlDataService sqlDataService;
    private ConfigurationService configurationService;

    public PdfService(ITextPdfGeneratorService iTextPdfGeneratorService, SqlDataService sqlDataService, ConfigurationService configurationService) {
        this.iTextPdfGeneratorService = iTextPdfGeneratorService;
        this.sqlDataService = sqlDataService;
        this.configurationService = configurationService;
    }

    public PdfGenerationResult generatePdf(String configName, String communicationCode) throws PdfGenerationException {
        Assert.notNull(configName, "ConfigName is not available");
        Assert.notNull(communicationCode, "CommunicationCode is not available");

        Properties data = null;

        Configuration conf = configurationService.getConfiguration(configName).orElseThrow(() -> new ConfigurationNotFoundException(configName));
        Assert.notNull(conf.getPersonalizationType(), "Configuration is wrong: PersonalizationType is not allowed to be null");

        if(conf.getDataProvider() == null){
            // Nothing to do here as template should be generated empty
        } else {
            if(conf.getDataProvider().getType().equals(DataProviderType.SQL)){
                data = sqlDataService.getDataByCommunicationCode(communicationCode, conf);
            }

            if(data == null || data.size() < 1){
                throw new DataNotFoundException(communicationCode);
            }
        }


        if(conf.getPersonalizationType() == PersonalizationType.FORM){
            PdfGenerationResult pdfGenerationResult = new PdfGenerationResult();
            pdfGenerationResult.setDownloadName(conf.getDownloadName());
            pdfGenerationResult.setPdf(generatePdfViaForm(new File(conf.getTemplate()), data));

            return pdfGenerationResult;
        } else {
            return null;
        }
    }

    private byte[] generatePdfViaForm(File template, Properties properties) throws PdfGenerationException {
        return iTextPdfGeneratorService.generatePdfFromAcroForm(template, properties);
    }
}
