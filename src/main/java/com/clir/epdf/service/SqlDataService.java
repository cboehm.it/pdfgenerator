package com.clir.epdf.service;

import com.clir.epdf.dto.Configuration;
import com.clir.epdf.Utilities;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Properties;

@Service
public class SqlDataService {
    private JdbcTemplate jdbcTemplate;

    public SqlDataService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Properties getDataByCommunicationCode(String communicationCode, Configuration configuration){
        Map<String, Object> sqlResult = jdbcTemplate.queryForMap(configuration.getDataProvider().getVal(), new Object[]{communicationCode});
        return Utilities.convertMapToProperties(sqlResult);
    }
}
